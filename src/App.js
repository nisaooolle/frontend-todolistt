import Registerr from "./pages/Registerr";
import Login from "./pages/Login";
import Presensi from "./pages/Presensi";
import Dasboard from "./pages/Dasboard";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./component/Home";
import Edit from "./pages/Edit";
import AbsenPulang from "./pages/AbsenPulang";
import Update from "./pages/Update";
import ProfilUser from "./pages/ProfileUser";

const App = () => {
  return (
    <div >
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/" component={Dasboard} exact />
            <Route path="/register" component={Registerr} exact />
            <Route path="/login" component={Login} exact />
            <Route path="/presensi" component={Presensi} exact />
            <Route path="/pulang" component={AbsenPulang} exact />
            <Route path="/todolist" component={Home} exact />
            <Route path="/profil" component={ProfilUser} exact />
            <Route path="/edit/:id" component={Edit} exact />
            <Route path="/update/:id" component={Update} exact />
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  );
};

export default App;
