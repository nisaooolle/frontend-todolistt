import axios from "axios";
import React, { useState } from "react";
import { Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/register.css";

const Registerr = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [fotoProfile, setFotoProfile] = useState(null);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [noTlpn, setNoTlpn] = useState("");
  const [show, setShow] = useState(false);
  const history = useHistory();
  const [role, setRole] = useState("USER");
  const register = async (e) => {
    e.preventDefault();

    // try catch untuk memastikan trjdi kesalahan
    try {
      // library opensource yg digunkan untuk request data melalui http
      await axios.post(
        "http://localhost:3005/user/sign-up",
        {
          email: email,
          password: password,
          fotoProfile: fotoProfile,
          nama: nama,
          alamat: alamat,
          noTlpn: noTlpn,
          role: role,
        }
        // headers berikut berfungs untuk method yg hanya diakses oleh admin
      );
      // Sweet alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Register!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/login");
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <body className="body md:text-base lg:h-100vh sm:text-sm">
      <div className="container">
        <h3>Registrasi</h3>
        <Form onSubmit={register} method="POST">
          <label>Email</label>
          <br />
          <input
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
            type="email"
          />
          <br />
          <label>Password</label>
          <br />
          <input
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
            type="password"
          />
          <br />

          <label>Nama</label>
          <br />
          <input
            value={nama}
            onChange={(e) => setNama(e.target.value)}
            required
            type="text"
          />
          <br />
          <label>Alamat</label>
          <br />
          <input
            value={alamat}
            onChange={(e) => setAlamat(e.target.value)}
            required
            type="text"
          />
          <br />
          <label>No tlpn</label>
          <br />
          <input
            value={noTlpn}
            onChange={(e) => setNoTlpn(e.target.value)}
            required
            type="number"
          />
          <br />
          <button type="submit">Register</button>
          <p>
            {" "}
            Sudah punya akun?
            <a href="/login">Login</a>
          </p>
        </Form>
      </div>
    </body>
  );
};

export default Registerr;
