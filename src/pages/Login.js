import axios from "axios";
import React, { useState } from "react";
import { Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:3005/user/sign-in",
        {
          email: email,
          password: password,
        }
      );
      // jika respon status 200 / ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasill!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("id", data.data.user.id);
        localStorage.setItem("token", data.data.token);
        localStorage.setItem("role", data.data.user.role);
        console.log(data);
        history.push("/");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };
  return (
    <body className="body1 md:text-base lg:h-100vh sm:text-sm">
      <div className="container1">
        <h3>Login</h3>
        <Form className="object-cover" onSubmit={login} method="POST">
          <label>Email</label>
          <br />
          <input
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <br />
          <label>Password</label>
          <br />
          <input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <br />
          <button type="submit">Log in</button>
          <p>
            {" "}
            Belum punya akun?
            <a href="/register">Register</a>
          </p>
        </Form>
      </div>
    </body>
  );
};

export default Login;
