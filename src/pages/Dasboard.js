import React from "react";
import "../style/dasboard.css";
import Image from "../img/undraw_Working_remotely_re_6b3a.png";
import { Navbar } from "../component/Navbar";
import Footer from "../component/Footer";
const Dasboard = () => {
  return (
    <body className="bo">
      <Navbar />
      <div className="lg:flex  sm:text-sm">
        <div className="lg:flex md:block">
          <img
            className="md:text-base sm:text-sm lg:w-2/4 mt-5"
            src={Image}
            alt=""
          />
          <div className="ml-10">
            {localStorage.getItem("id") !== null ? (
              <h2 className="mt-20 font-serif md:text-base lg:text-5xl sm:text-sm">
                let's attend and fill in the todolist
              </h2>
            ) : (
              <h2 className="mt-40 font-serif md:text-base lg:text-5xl sm:text-sm">
                Please login to fill in attendance{" "}
                <h2 className="lg:text-3xl  md:text-base sm:text-sm font-serif  text-blue-800">
                  and todo-lists
                </h2>
              </h2>
            )}
            <div className="lg:flex mt-5 sm:text-sm lg:text-2xl md:block ">
              {localStorage.getItem("id") !== null ? (
                <div class="w-64 max-w-sm p-3 mr-11 bg-white border border-gray-200 rounded-lg shadow-2xl shadow-black dark:bg-gray-800 dark:border-gray-700">
                  <img
                    className="w-full h-28"
                    src="https://www.talenta.co/wp-content/uploads/2019/10/Talenta-Blog_Absensi_Karyawan_Secara_Online_dan_Manual_Mana_Yang_Lebih_Menguntungkan_HEADER.png"
                    alt=""
                  />
                  <a href="#">
                    <h5 class="mb-2 text-2xl font-semibold tracking-tight text-gray-800 dark:text-white">
                      Klik ikon di bawah untuk mengisi kehadiran
                    </h5>
                  </a>
                  <a
                    href="/presensi"
                    class="text-gray-500 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md font-medium"
                  >
                    <i class="fa-sharp fa-solid fa-person-chalkboard"></i>
                  </a>
                </div>
              ) : (
                <></>
              )}
              <br />
              {localStorage.getItem("id") !== null ? (
                <div class="w-64 max-w-sm p-3 mr-10 bg-white border border-gray-200 rounded-lg shadow-2xl shadow-black  dark:bg-gray-800 dark:border-gray-700 ">
                  <img
                    className="w-full h-28"
                    src="https://itsfoss.com/content/images/wordpress/2020/06/open-Source-to-do-list-apps.jpg"
                    alt=""
                  />
                  <a href="#">
                    <h5 class="mb-2 text-1xl font-semibold tracking-tight text-gray-800 dark:text-white">
                      Klik ikon di bawah untuk mengisi data harian
                    </h5>
                  </a>
                  <a
                    href="/todolist"
                    class="text-gray-500 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md font-medium"
                  >
                    <i class="fa-solid fa-list"></i>
                  </a>
                </div>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      </div>
      <br />
      <Footer />
    </body>
  );
};

export default Dasboard;
