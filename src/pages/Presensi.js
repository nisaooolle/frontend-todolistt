import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import { Navbar } from "../component/Navbar";
import ReactPaginate from "react-paginate";
import { useHistory, useParams } from "react-router-dom";

const Presensi = () => {
  const [presen, setPresen] = useState([]); //useState berfungsi untuk menyimpan data sementara
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false); // fungsi handleClose akan menyetel variabel acara ke false.
  const handleShow = () => setShow(true); //  menyetel variabel status acara ke true,
  const [pages, setPages] = useState(0);
  const [status, setStatus] = useState([]);

  const getAll = async (page = 0) => {
    await axios
      .get(
        `http://localhost:3005/presensi1/all-presensi?userId=${localStorage.getItem(
          "id"
        )}&page=${page}`
      )
      .then((res) => {
        setPresen(res.data.data.content);
        setPages(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi kesalahan" + error);
      });
  };

  useEffect(() => {
    //mengambil data, memperbarui DOM secara langsung,
    getAll(0);
  }, []);

  const addUser = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("status", status);

    try {
      // library opensource yg digunkan untuk request data melalui http
      await axios.post(
        `http://localhost:3005/presensi1/post?userId=${localStorage.getItem(
          "id"
        )}`,
        formData
      );
      sessionStorage.setItem("kondisi", "sudah");
      // Sweet alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Absen Masuk!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {}, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  const deleteUser = async (id) => {
    Swal.fire({
      title: "Yakin ingin menghapus data ini?",
      text: "Data kamu tidak akan bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3005/presensi1/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
        setTimeout(() => {
          // window.location.reload();
        }, 1000);
      }
    }); // untuk pemberitahuan jika sudah berhasil menghapus
    getAll(0);
  };
  return (
    <div>
      <Navbar />
      <h1 className="font-serif md:text-base lg:text-4xl sm:text-sm text-center mt-6">
        Table Absensi Masuk
      </h1>
      <div className="flex gap-5 md:flex justify-between items-center md:text-base lg:ml-10">
        <button className="btn btn-primary w-60 " onClick={handleShow}>
          <a href="/presensi" className="text-white">
            ABSEN MASUK
          </a>
          <p>
            {sessionStorage.getItem("kondisi") === null ? (
              <>Belum Absen</>
            ) : (
              <>Sudah Absen</>
            )}
          </p>
        </button>
        <button className="btn btn-outline-primary lg:mr-10 w-60">
          <a href="/pulang" className="hover:text-white text-blue-900">
            ABSEN PULANG
          </a>
          <p className=" text-blue-900">
            {sessionStorage.getItem("kondisii") === null ? (
              <>Belum Absen</>
            ) : (
              <>Sudah Absen</>
            )}
          </p>
        </button>
      </div>
      <br />
      <div class="relative overflow-x-auto shadow-md sm:text-sm font-serif md:text-base lg:text-5xl flex-wrap ">
        <table class="table md:w-full sm:text-sm lg:text-left text-gray-500 dark:text-gray-400 ">
          <thead class="lg:text-xs sm:text-sm md:text-base uppercase dark:bg-gray-700 dark:text-gray-400 bg-blue-900 text-white text-justify p-5 rounded ">
            <tr className="text-center">
              <th scope="col" class="px-6 py-3">
                No
              </th>
              <th scope="col" class="px-6 py-3">
                Tanggal
              </th>
              <th scope="col" class="px-6 py-3">
                Absen Masuk
              </th>
              <th scope="col" class="px-6 py-3">
                Status
              </th>
              <th>action</th>
            </tr>
          </thead>
          <tbody className="text-center lg:text-xl ">
            {presen.map(
              (
                presen,
                index //map untuk memetakan data
              ) => (
                <tr key={presen.id}>
                  <td className="shadow-2xl shadow-blue-900">{index + 1}</td>
                  <td>{presen.tanggal}</td>
                  <td>{presen.masuk}</td>
                  <td>{presen.status}</td>
                  {localStorage.getItem("id") !== null ? (
                    <td className="data">
                      <button //button klik untuk delete
                        className=" text-red-500"
                        onClick={() => deleteUser(presen.id)}
                      >
                        {" "}
                        <i class="fa-solid fa-trash"></i>
                      </button>
                      <a href={"/update/" + presen.id}>
                        <button //button klik untuk edit
                          variant="warning"
                          className=" text-green-400 ml-10"
                        >
                          <i class="fa-solid fa-pen-to-square"></i>
                        </button>
                      </a>
                    </td>
                  ) : (
                    <></>
                  )}
                </tr>
              )
            )}
          </tbody>
        </table>
        {presen.length == 0 ? (
          <p align="center">Product tidak tersedia</p>
        ) : (
          <div>
            <ReactPaginate
              previousLabel={"previous"}
              nextLabel={"next"}
              breakLabel={"..."}
              pageCount={pages}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              onPageChange={(e) => getAll(e.selected)}
              containerClassName={"pagination d-flex justify-content-center"}
              pageClassName={"page-item"}
              pageLinkClassName={"page-link"}
              previousClassName={"page-item"}
              previousLinkClassName={"page-link"}
              nextClassName={"page-item"}
              nextLinkClassName={"page-link"}
              breakClassName={"page-item"}
              breakLinkClassName={"page-link"}
              activeClassName={"active"}
            />
          </div>
        )}
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-blue-900" closeButton>
          <Modal.Title className="text-white">Absensi</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={addUser}>
            <div className="mb-3">
              <Form.Label>
                <strong>Status</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Massukan status"
                  type="text"
                  value={status}
                  onChange={(e) => setStatus(e.target.value)}
                />
              </InputGroup>
            </div>
            <a href="/presensi" className="btn btn-danger">
              Close
            </a>
            ||
            <button className="btn btn-primary" onClick={handleClose}>
              Save
            </button>
          </Form>
        </Modal.Body>
      </Modal>
      <br />
      <div className="ml-10 text-blue-900">
        <h1>KETERANGAN = </h1>
        <h2>Jam Masuk: 08.00</h2>
        <h2>Jam Pulang: 17.00</h2>
      </div>
    </div>
  );
};

export default Presensi;
