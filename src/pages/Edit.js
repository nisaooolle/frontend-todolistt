import axios from "axios";
import React, { useEffect, useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function () {
  const param = useParams();
  const [task, setTask] = useState("");

  const history = useHistory();

  const updateT = async (e) => {
    e.preventDefault();

    Swal.fire({
      title: "Yakin Ingin mengubah data ini?",
      text: "Anda tidak akan dapat mengembalikan data ini!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, edit it!",
    })
      .then((result) => {
        if (result.isConfirmed) {
          axios.put("http://localhost:3005/todolist1/" + param.id, {
            task: task,
            userId: localStorage.getItem("userId"),
          });
        }
      })
      .then(() => {
        history.push("/todolist");
        Swal.fire("Berhasil!!", "Data kamu berhasil di update,", "success");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      })
      .catch((error) => {
        alert("Terjadinya kesalahan: " + error);
      });
  };

  useEffect(() => {
    axios
      .get("http://localhost:3005/todolist1/" + param.id)
      .then((response) => {
        const newMenu = response.data.data;
        setTask(newMenu.task);
      })
      .catch((error) => {
        alert("Terjadi kesalahan Lurr!!! " + error);
      });
  }, []);
  return (
    <body className="body1 md:text-base sm:text-sm">
      <div className="edit mx-5">
        <div
          style={{ padding: 10, width: 400, height: 250 }}
          className="container1 "
        >
          <div className="">
            <Form onSubmit={updateT}>
              <h2 className="text-center text-xl text-white">Update Task :</h2>
              <hr />
              <div>
                <div style={{ marginTop: 30 }}>
                  <div className="name ">
                    <Form.Label>
                      <strong>Nama </strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                      <Form.Control
                        placeholder="Masukkan teks "
                        value={task}
                        onChange={(e) => setTask(e.target.value)}
                      />
                    </InputGroup>
                  </div>
                </div>
              </div>

              <div className="d-flex justify-content-end align-items-center mt-2">
                <button style={{ width: 600 }} className="btn btn-primary">
                  Save
                </button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </body>
  );
}
