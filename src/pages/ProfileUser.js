import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import { Navbar } from "../component/Navbar";
import Swal from "sweetalert2";

const ProfileUser = () => {
  const [profile, setProfile] = useState({
    fotoProfile: null,
    nama: "",
    alamat: "",
    email: "",
    noTlpn: "",
    password: "",
  });
  const [email, setEmail] = useState("");
  const [fotoProfile, setFotoProfile] = useState("");
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [noTlpn, setNoTlpn] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false); // fungsi handleClose akan menyetel variabel acara ke false.
  const handleShow = () => setShow(true); //  menyetel variabel status acara ke true,

  const getAll = async () => {
    await axios
      .get("http://localhost:3005/user/" + localStorage.getItem("id"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi kesalahan" + error);
      });
  };

  useEffect(() => {
    //mengambil data, memperbarui DOM secara langsung,
    getAll(0);
  }, []);

  useEffect(() => {
    //mengambil data, memperbarui DOM secara langsung,
    axios
      .get("http://localhost:3005/user/" + localStorage.getItem("id"))
      .then((response) => {
        const profil = response.data.data;
        setFotoProfile(profil.fotoProfile);
        setNama(profil.nama);
        setAlamat(profil.alamat);
        setNoTlpn(profil.noTlpn);
        setEmail(profil.email);
        setPassword(profil.password);
      })
      .catch((error) => {
        alert("Terjadi kesalahan Sir!!" + error);
      });
  }, []);
  const putProfile = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", fotoProfile);
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("noTlpn", noTlpn);
    formData.append("email", email);
    formData.append("password", password);

    try {
      // library opensource yg digunkan untuk request data melalui http
      await axios.put(
        `http://localhost:3005/user/${localStorage.getItem("id")}`,
        formData
        // headers berikut berfungs untuk method yg hanya diakses oleh admin
      );
      // Sweet alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil mengedit profil!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {}, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      {/* <Navbar/> */}
      <div className="bg-[url('/src/img/background.jpg')] p-10 h-60 md:w-1/1  bg-cover">
        <h2 className="text-white flex text-2xl">
          Profil Saya{" "}
          <a href="/" className="ml-auto text-2xl">
            <i class="fa-solid fa-backward"></i>
          </a>
        </h2>
        <p className="">
          Kelola informasi profil Anda untuk mengontrol, melindungi dan
          mengamankan akun
        </p>
        <div className="shadow-2xl shadow-black md:flex backdrop-opacity-10 backdrop-invert bg-white/30 rounded-xl p-8 dark:bg-slate-800 md:w-1/2 md:ml-60 mt-10">
          <div className="flex-none w-56 ">
            <img
              className="w-24 h-24 md:w-64 md:h-64 md:rounded-full rounded-full mx-auto shadow-2xl shadow-blue-800"
              src={
                !profile.fotoProfile
                  ? "https://cdn4.iconfinder.com/data/icons/instagram-ui-twotone/48/Paul-18-512.png"
                  : profile.fotoProfile
              }
              alt=""
              width={300}
              height={250}
              type="file"
            />
          </div>
          <form className="flex-auto p-6">
            <div className="flex flex-wrap">
              <h1 className="flex-auto font-medium text-slate-900 text-2xl">
                {profile.nama}
              </h1>
            </div>
            <hr />
            <div className="w-full flex-none mt-2 order-1 text-2xl font-bold ">
              <p className="text-violet-600">
                <i class="fa-solid fa-envelope"></i> {profile.email}
              </p>
            </div>
            <div className="d-flex text-black">
              <p className="text-black">
                <i class="fa-solid fa-location-dot"></i> {profile.alamat}
              </p>
            </div>
            <div>
              <p className=" font-medium text-violet-600">
                <i class="fa-solid fa-phone-volume"></i> {profile.noTlpn}
              </p>
              <br />
            </div>
            <Button
              onClick={handleShow}
              className="sim text-black hover:text-white  bg-gradient-to-r from-violet-500 to-fuchsia-500 "
              type="button"
            >
              edit profile
            </Button>
          </form>

          {/* </Form> */}
          <Modal show={show} onHide={handleClose}>
            <Modal.Header
              className="add"
              closeButton
              style={{ backgroundColor: "#2B3A55", color: "white" }}
            >
              <Modal.Title>edit profile</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form onSubmit={putProfile}>
                <div class="mb-4">
                  <label class="block text-gray-700 text-sm font-bold mb-2">
                    Foto Profile
                  </label>
                  <input
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    onChange={(e) => setFotoProfile(e.target.files[0])}
                    type="file"
                    placeholder="Profil"
                  />
                </div>
                <div class="mb-4">
                  <label class="block text-gray-700 text-sm font-bold mb-2">
                    Nama
                  </label>
                  <input
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    type="nama"
                    placeholder="Nama"
                    value={nama}
                    onChange={(e) => setNama(e.target.value)}
                  />
                </div>
                <div class="mb-4">
                  <label class="block text-gray-700 text-sm font-bold mb-2">
                    Email
                  </label>
                  <input
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    type="email"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>

                <div class="mb-4">
                  <label class="block text-gray-700 text-sm font-bold mb-2">
                    Alamat
                  </label>
                  <input
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    type="alamat"
                    placeholder="Alamat"
                    value={alamat}
                    onChange={(e) => setAlamat(e.target.value)}
                  />
                </div>
                <div class="mb-4">
                  <label class="block text-gray-700 text-sm font-bold mb-2">
                    No Telpon
                  </label>
                  <input
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    type="telepon"
                    placeholder="Telepon"
                    value={noTlpn}
                    onChange={(e) => setNoTlpn(e.target.value)}
                  />
                </div>
                <div class="mb-4">
                  <label class="block text-gray-700 text-sm font-bold mb-2">
                    Password
                  </label>
                  <input
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
                <button
                  className="mx-1 button-btl btn btn-danger"
                  onClick={handleClose}
                >
                  Close
                </button>
                <button
                  className="mx-1 button btn btn-primary"
                  onClick={handleClose}
                >
                  Save
                </button>
              </Form>
            </Modal.Body>
          </Modal>
        </div>
      </div>
    </div>
  );
};

export default ProfileUser;
