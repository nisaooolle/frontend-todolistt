import axios from "axios";
import React, { useEffect, useState } from "react";
import { Form, InputGroup, Modal } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

const Update = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false); // fungsi handleClose akan menyetel variabel acara ke false.
  const handleShow = () => setShow(true); //  menyetel variabel status acara ke true,

  const [status, setStatus] = useState([]);
  const param = useParams();

  const history = useHistory();
  const update = async (e) => {
    e.preventDefault();

    // const fromData = new FormData();
    // fromData.append("nama", nama);
    // fromData.append("status", status);

    Swal.fire({
      title: "Yakin Ingin mengubah data ini?",
      text: "Anda tidak akan dapat mengembalikan data ini!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, edit it!",
    })
      .then((result) => {
        if (result.isConfirmed) {
          axios.put("http://localhost:3005/presensi1/" + param.id, {
            status: status,
            userId: localStorage.getItem("userId"),
          });
        }
      })
      .then(() => {
        history.push("/presensi");
        Swal.fire("Berhasil!!", "Data kamu berhasil di update,", "success");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      })
      .catch((error) => {
        alert("Terjadinya kesalahan: " + error);
      });
  };

  useEffect(() => {
    axios
      .get("http://localhost:3005/presensi1/" + param.id)
      .then((response) => {
        const newMenu = response.data.data;
        setStatus(newMenu.status);
      })
      .catch((error) => {
        alert("Terjadi kesalahan Lurr!!! " + error);
      });
  }, []);
  return (
    <body className="body1 md:text-base sm:text-sm">
      {" "}
      <div
        style={{ padding: 10, width: 400, height: 250 }}
        className="container1"
      >
        <div className="edit mx-5">
          <Form onSubmit={update}>
            <h2 className="text-center text-xl text-white">Update Status :</h2>
            <hr />
            <div>
              <div style={{ marginTop: 30 }}>
                <div className="name ">
                  <Form.Label>
                    <strong>Status </strong>
                  </Form.Label>
                  <InputGroup className="d-flex gap-3">
                    <Form.Control
                      placeholder="Masukkan teks "
                      value={status}
                      onChange={(e) => setStatus(e.target.value)}
                    />
                  </InputGroup>
                </div>
              </div>
            </div>

            <div className="d-flex justify-content-end align-items-center mt-2">
              <button style={{ width: 600 }} className="btn btn-primary">
                Save
              </button>
            </div>
          </Form>
        </div>
      </div>
    </body>
  );
};

export default Update;
