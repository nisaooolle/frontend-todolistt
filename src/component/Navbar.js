import React from "react";
import { useHistory } from "react-router-dom";

export const Navbar = () => {
  const history = useHistory();

  const logout = () => {
    window.location.reload();
    localStorage.clear();
    history.push("/");
  };
  return (
    <div>
      <div className="flex bg-blue-900 text-gray-200 font-mono p-4 h-20 shadow-xl shadow-blue-700">
        <div className="text-center">
          <h1>
            <a href="/" className="flex">
              <img
                className="w-14"
                src="https://cdn-icons-png.flaticon.com/512/5058/5058451.png"
                alt=""
              />
              <b className="font-serif lg:text-2xl mt-2 text-white">
                {" "}
                Target Audience
              </b>
            </a>
          </h1>
        </div>
        {localStorage.getItem("id") !== null ? (
          <>
            <div className="ml-auto mr-10 mt-2 lg:text-2xl md:text-base sm text-sm">
              <a className="text-white" href="/profil">
                <i class="fa-solid fa-user"></i>{" "}
              </a>
            </div>
            <div className=" mt-2 lg:text-2xl md:text-base sm text-sm">
              <a onClick={logout} href="/login">
                <i class="fas fa-sign-out-alt"></i>
              </a>
            </div>
          </>
        ) : (
          <div className="ml-auto mr-10 mt-2 lg:text-2xl md:text-base sm text-sm ">
            <a className="text-white" href="/login">
              <i class="fas fa-sign-in-alt"></i>
            </a>
          </div>
        )}
      </div>
    </div>
  );
};
