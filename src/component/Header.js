import React from "react";

const Header = () => {
  return (
    <div className="todo">
      <h1 className="lg:text-4xl sm:text-sm md:text-base text-center mb-5 font-serif text-white ">
        Todos list
      </h1>
    </div>
  );
};

export default Header;
