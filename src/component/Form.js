import axios from "axios";
import React, { useEffect } from "react";
import Swal from "sweetalert2";

const Form = ({ input, setInput, todos, setTodos, editTodo, SetEditTodo }) => {
  const updateTodo = (title, id, completed) => {
    const newTodo = todos.map((todo) =>
      todo.id === id ? { title, id, completed } : todo
    );

    setTodos(newTodo);
    SetEditTodo("");
  };

  useEffect(() => {
    if (editTodo) {
      setInput(editTodo.title);
    } else {
      setInput("");
    }
  }, [setInput, editTodo]);

  const onInputChange = (event) => {
    setInput(event.target.value);
  };

  const onFormSubmit = async (event) => {
    event.preventDefault();
    if (!editTodo) {
      try {
        await axios.post(`http://localhost:3005/todolist1/post`, {
          userId: localStorage.getItem("id"),
          task: input,
        });
        Swal.fire({
          icon: "success",
          title: "Berhasil menambahkan task!",
          showConfirmButton: false,
          timer: 1500,
        });
        window.location.reload();
      } catch (error) {
        console.log(error);
      }
    } else {
      updateTodo(input, editTodo.id, editTodo.completed);
    }
  };
  return (
    <div>
      <form onSubmit={onFormSubmit}>
        <input
          type="text"
          placeholder="Enter a Todo..."
          className="task-input lg:text-lg sm:text-sm md:text-base"
          value={input}
          onChange={onInputChange}
          required
        />
        <button className="button-add" type="submit">
          {editTodo ? "OK" : "Add"}
        </button>
      </form>
    </div>
  );
};

export default Form;
