import React, { useEffect, useState } from "react";
import Header from "./Header";
import Form from "./Form";
import TodosList from "./TodosList";
import "../style/home.css";
import { Navbar } from "./Navbar";
import axios from "axios";
import Image from "../img/undraw_Learning_re_32qv (1).png";
// home
export default function Home() {
  const [input, setInput] = useState("");
  const [todos, setTodos] = useState([]);
  const [editTodo, setEditTodo] = useState(null);

  const getAll = async () => {
    await axios
      .get(
        `http://localhost:3005/todolist1/all-todolist?userId=${localStorage.getItem(
          "id"
        )}`
      )
      .then((res) => {
        setTodos(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi kesalahan" + error);
      });
  };

  useEffect(() => {
    //mengambil data, memperbarui DOM secara langsung,
    getAll(0);
  }, []);

  return (
    <div>
      <Navbar />
      <div className="block5 mt-16 ">
        <img
          className="md:text-base sm:text-sm lg:w-2/4 mt-5 mr-32"
          src={Image}
          alt=""
        />
        <div className="app-wrapper">
          <div>
            <Header />
          </div>
          <div>
            <Form
              input={input}
              setInput={setInput}
              todos={todos}
              setTodos={setTodos}
              editTodo={editTodo}
              setEditTodo={setEditTodo}
            />
          </div>
          <div>
            <TodosList
              todos={todos}
              setTodos={setTodos}
              setEditTodo={setEditTodo}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
