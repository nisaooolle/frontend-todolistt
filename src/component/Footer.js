import React from "react";

const Footer = () => {
  return (
    <div className="shadow-2xl shadow-blue-900 backdrop-blur-lg bg-blue-900 ">
      <footer class="p-4 rounded-lg md:px-6 md:py-8 dark:bg-gray-900 shadow-blue-100">
        <div class="sm:flex sm:items-center sm:justify-between">
          <a
            href="https://flowbite.com/"
            class="flex items-center mb-4 sm:mb-0"
          >
            <a href="/" className="flex">
              <img
                className="w-14"
                src="https://cdn-icons-png.flaticon.com/512/5058/5058451.png"
                alt=""
              />
              <b className="font-serif lg:text-2xl mt-3 text-white shadow-2xl shadow-blue-900">
                {" "}
                Target Audience
              </b>
            </a>
          </a>
          <ul class="flex flex-wrap items-center mb-6 text-white-500 sm:mb-0 dark:text-gray-400 shadow-2xl shadow-blue-900">
            <li>
              <a
                href="https://github.com/"
                class="mr-4 hover:underline md:mr-6 text-white lg:text-2xl "
              >
                <i class="fa-brands fa-github"></i>
              </a>
            </li>
            <li>
              <a
                href="https://www.instagram.com/?hl=id"
                class="mr-4 hover:underline md:mr-6 text-white lg:text-2xl"
              >
                <i class="fa-brands fa-instagram"></i>
              </a>
            </li>
            <li>
              <a
                href="https://www.google.com/intl/id/gmail/about/"
                class="mr-4 hover:underline md:mr-6 text-white lg:text-2xl"
              >
                <i class="fa-solid fa-envelope"></i>
              </a>
            </li>
            <li>
              <a
                href="https://web.whatsapp.com/"
                class="hover:underline text-white lg:text-2xl"
              >
                <i class="fa-brands fa-whatsapp"></i>
              </a>
            </li>
          </ul>
        </div>
        <hr class="my-6 border-blue-200 sm:mx-auto dark:border-blue-400 lg:my-8 " />
        <span class="block text-sm text-white sm:text-center dark:text-gray-400">
          © 3{" "}
          <a href="https://flowbite.com/" class="hover:underline text-gray-400">
            Flowbite™
          </a>
          . All Rights Reserved.
        </span>
      </footer>
    </div>
  );
};

export default Footer;
