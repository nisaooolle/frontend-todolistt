import axios from "axios";
import React from "react";
import Swal from "sweetalert2";
import "../style/todolist.css";

const TodosList = ({ todos, setTodos }) => {
  const handledelete = ({ id }) => {
    setTodos(todos.filter((todo) => todo.id !== id));
    Swal.fire({
      title: "Yakin ingin menghapus data ini?",
      text: "Data kamu tidak akan bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3005/todolist1/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
        setTimeout(() => {
          // window.location.reload();
        }, 1000);
      }
    }); // untuk pemberitahuan jika sudah berhasil menghapus
  };
  const validasi = async (id) => {
    try {
      await axios.put(`http://localhost:3005/todolist1/selesai/` + id);
      // Sweet alert
      Swal.fire({
        icon: "success",
        title: "Selesai!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {}, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="todo">
      <div>
        {todos.map((todo) => (
          <li
            className="list-item lg:text-lg sm:text-sm md:text-base"
            key={todo.id}
          >
            <input
              type="text"
              value={todo.task}
              // className={`list ${
              //   todo.completed ? "complete" : ""
              // } rounded-lg bg-rose-300/70`}
            />
            <div className="flex gap-3">
              <p>{todo.selesai}</p>
              <div class="flex items-center h-5">
                <button
                  className="text-pink-500"
                  onClick={() => validasi(todo.id)}
                >
                  <i class="fa-solid fa-check"></i>
                </button>
              </div>
              <a href={"/edit/" + todo.id}>
                <button className=" button-edit task-button">
                  <i class="fas fa-edit"></i>
                </button>
              </a>
              <button
                className="button-delete task-button"
                onClick={() => handledelete(todo)}
              >
                <i class="fa-solid fa-trash"></i>
              </button>
            </div>
          </li>
        ))}
      </div>
    </div>
  );
};

export default TodosList;
